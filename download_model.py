import argparse
from comet_ml import API


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--workspace", type=str)
    parser.add_argument("--model_name", type=str)
    parser.add_argument("--model_version", type=str, default="1.0.0")
    parser.add_argument("--output_path", type=str, default="./")

    return parser.parse_args()


def download_model(workspace, model_name, model_version, output_path="./"):
    api = API()
    api.download_registry_model(
        workspace,
        registry_name=model_name,
        version=model_version,
        output_path=output_path,
    )


def main():
    args = get_args()

    workspace = args.workspace
    model_name = args.model_name
    model_version = args.model_version
    output_path = args.output_path

    download_model(workspace, model_name, model_version, output_path)


if __name__ == "__main__":
    main()
